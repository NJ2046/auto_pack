import pytest

from utils.time import get_local_ymd_date

def get_local_ymd_date():
    expected = str

    result = get_local_ymd_date()
    assert isinstance(result, expected)
