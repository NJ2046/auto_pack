import time
import datetime
import random


def get_local_ymd_date() -> str:
    """获取北京年月日
    Args:


    Returns:
            yyyymmdd(str)
    """
    time_now = time.strftime('%Y%m%d',time.localtime(time.time()))
    return time_now
